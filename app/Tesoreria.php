<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tesoreria extends Model
{
    //
    public $timestamps = false;
    protected $fillable = [
    	'nombre',
    	'telefono',
    	'fecha',
    	'description',
    	'precio',
    	'fotoFactura',
    ];
    protected $table= 'tesoreria';
}
