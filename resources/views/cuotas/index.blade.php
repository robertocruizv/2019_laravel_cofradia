@extends('layouts.layout_jorge')
@section('contenido')
<a href="cuotas/create/"><button type="button" class="btn btn btn-success">Añadir</button></a>
<a href="{{ url('dynamic_pdf/pdf') }}" class="btn btn-danger">Convertir a PDF</a>
<div class="table-responsive">
	<table class="table table-dark table-hover table-striped">
		<thead class="thead-dark">
			<th>Nombre</th>
			<th>Cantidad</th>
			<th>Concepto</th>
			<th>Fecha</th>
			<th>Mostrar</th>
			<th>Editar</th>
			<th>Eliminar</th>
		</thead>
		<tbody>
			<?php foreach ($listadoCuotas as $x) { ?>
			<tr>
				<td> <?php echo $x->nombre;?></td>
				<td> <?php echo $x->valor;?></td>
				<td><?php echo $x->concepto;?></td>
				<td><?php echo $x->fecha_cobro;?></td>
				<td><a href="cuotas/show/<?php echo $x->id ?>"><button type="button" class="btn btn-primary">Mostrar</button></a></td>
				<td><a href="cuotas/edit/<?php echo $x->id ?>"><button type="button" class="btn btn-info">Editar</button></a></td>
				<td>
					<form method="post" action="{{ route('cuotas.destroy', $x->id) }}">
						@method('DELETE')
					@csrf
					<button type="submit" class="btn btn-danger">Eliminar</button>
					</form>
				</td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
@stop
